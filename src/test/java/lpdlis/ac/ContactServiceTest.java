package lpdlis.ac;

import org.junit.Test;

public class ContactServiceTest {

    IContactService service = new ContactService();

    @Test(expected = ContactException.class)
    public void shouldFailIfNull() throws ContactException{ 
        service.creerContact(null);
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfEmpty() throws ContactException{ 
        service.creerContact("   ");
    }

    @Test(expected = ContactException.class)
    public void shouldFailIfTooLong() throws ContactException{ 
        service.creerContact("azertyuiopqsdfghjklmwxcvbnaqwzsxedcrfvtgb");
    }

    @Test
    public void shouldSucceed() throws ContactException{ 
        service.creerContact("Thierry");
    }
}
