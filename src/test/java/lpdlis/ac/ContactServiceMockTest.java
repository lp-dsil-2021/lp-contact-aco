package lpdlis.ac;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {
    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Captor
    private ArgumentCaptor<Contact> contactCaptor;

    @Test(expected = ContactException.class)
    // Test d'ajout d'un duplicata
    public void shouldFailOnDuplicateEntry() throws ContactException {
        //Definition du Mock
        Mockito.when(contactDao.isContactExists("Thierry")).thenReturn(true);
        //test
        contactService.creerContact("Thierry");
    }

    @Test
    // Test ajout non inclus dans la liste
    public void shouldSucceed() throws ContactException {
        //Definition du Mock
        Mockito.when(contactDao.isContactExists("Thierry")).thenReturn(false);
        //test
        contactService.creerContact("Thierry");
    }

    @Test(expected = ContactException.class)
    //Test d'un ajout d'une valeur null
    public void shouldFailIfNull() throws ContactException{ 
        contactService.creerContact(null);
    }

    @Test(expected = ContactException.class)
    //Test d'ajout d'un string vide
    public void shouldFailIfEmpty() throws ContactException{ 
        contactService.creerContact("   ");
    }

    @Test(expected = ContactException.class)
    //Ajout d'un string > 40 caractères
    public void shouldFailIfTooLong() throws ContactException{ 
        contactService.creerContact("azertyuiopqsdfghjklmwxcvbnaqwzsxedcrfvtgb");
    }

    @Test
    // test d'ajout d'un string de 3 caractères
    public void shouldSucceed2() throws ContactException{ 
        contactService.creerContact("leo");
    }

    @Test
    // test d'ajout d'un string de 40 caractères
    public void shouldSucceed3() throws ContactException{
        contactService.creerContact("maximumcharacterssssssssssssssssssssss");
    }

    @Test(expected = ContactException.class)
    // test d'ajout d'un string contenant des chiffres
    public void shouldFailWithDigit() throws ContactException{
        contactService.creerContact("Thierry56");
    }

    @Test(expected = ContactException.class)
    // test d'ajout d'un string contenant des caractères spéciaux
    public void shouldFailWithSpecialChar() throws ContactException{
        contactService.creerContact("Thierry$!");
    }

    @Test
    // test d'ajout d'un string contenant des chiffres
    public void shouldSucceedWithComposedName() throws ContactException{
        contactService.creerContact("Jean-claude");
    }

    @Test
    public void shouldPass() throws ContactException {
        contactService.creerContact("       Thierry           ");

        Mockito.verify(contactDao).addToList(contactCaptor.capture());
        // Je souhaite connaitre le contenu de l'argument
        Contact contact = contactCaptor.getValue();
        Assert.assertEquals("Thierry", contact.getNom());

    }
}
