package lpdlis.ac;

public class Contact {

    private String nom;
    
    public Contact(String nouveauNom) {
        this.nom = nouveauNom;
    }

    public String getNom() {
        return this.nom;
    }
}
