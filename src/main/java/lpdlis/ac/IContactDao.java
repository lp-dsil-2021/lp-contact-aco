package lpdlis.ac;

//import org.mockito.stubbing.OngoingStubbing;

public interface IContactDao {

    public boolean isContactExists(String name);

    public void addToList(Contact contact);
}
