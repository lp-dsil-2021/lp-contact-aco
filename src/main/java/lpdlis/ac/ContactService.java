package lpdlis.ac;

public class ContactService implements IContactService {

    private IContactDao contactDao = new ContactDao();

    // List<String> listeNom = Arrays.asList("Michel");

    @Override
    public Contact creerContact(String nom) throws ContactException {

        if (nom == null || nom.trim().length() < 3 || nom.trim().length() > 40) {
            throw new ContactException();
        }

        if (contactDao.isContactExists(nom)) {
            throw new ContactException();
        }

        if(nom.matches(".*\\d.*")) {
            throw new ContactException();
        }

        if(nom.matches(".*[^A-Za-z- ].*")) {
            throw new ContactException();
        }

        nom = nom.strip();
        
        Contact contact = new Contact(nom);
        contactDao.addToList(contact);
        return contact;
    }
}

