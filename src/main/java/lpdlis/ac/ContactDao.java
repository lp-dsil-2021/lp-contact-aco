package lpdlis.ac;

import java.util.*;

public class ContactDao implements IContactDao {

    private List<Contact> listeNom = new ArrayList<>();

    @Override
    public boolean isContactExists(String name) {
        for (Contact contact : listeNom){
            if(name.equalsIgnoreCase(contact.getNom())){
                return true;
            }
        }
        return false;
    }

    public List<Contact> getList() {
        return this.listeNom;
    }

    public void addToList(Contact contact){
        this.listeNom.add(contact);
    }
}
